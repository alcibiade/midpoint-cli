import time
from dataclasses import dataclass
from typing import Optional, Tuple, Type
from urllib.parse import urljoin
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

import requests
from midpoint_cli.client.objects import (namespaces, MidpointObjectList, MidpointTask, MidpointResource,
                                         MidpointConnector, MidpointUser, MidpointOrganization, MidpointObjectTypes,
                                         MidpointObjectType)
from midpoint_cli.client.observer import MidpointCommunicationObserver
from midpoint_cli.client.patch import patch_from_file
from midpoint_cli.client.progress import AsciiProgressMonitor
from midpoint_cli.client.session import CustomRetryManager, CustomHTTPAdapter


class MidpointServerError(Exception):
    pass


class MidpointUnsupportedOperation(Exception):
    pass


@dataclass
class MidpointClientConfiguration:
    url: str = "http://localhost:8080/midpoint/"
    username: str = "administrator"
    password: str = "5ecr3t"


class RestApiClient:
    def __init__(self, client_configuration: MidpointClientConfiguration,
                 observer: MidpointCommunicationObserver = None):
        self.configuration = client_configuration

        if observer is None:
            # No-op observer implementation
            observer = MidpointCommunicationObserver()

        session = requests.Session()

        adapter = CustomHTTPAdapter(max_retries=CustomRetryManager(connect=1000, total=1000, observer=observer),
                                    observer=observer)
        session.mount('http://', adapter)
        session.mount('https://', adapter)

        self.requests_session = session

    def get_element(self, element_class: MidpointObjectType, element_oid: str) -> Optional[str]:
        response = self.requests_session.get(
            url=urljoin(self.configuration.url, 'ws/rest/' + element_class.endpoint + '/' + element_oid),
            auth=(self.configuration.username, self.configuration.password))

        return response.content.decode()

    def delete(self, element_class: MidpointObjectType, element_oid: str) -> str:
        response = self.requests_session.delete(
            url=urljoin(self.configuration.url, 'ws/rest/' + element_class.endpoint + '/' + element_oid),
            auth=(self.configuration.username, self.configuration.password))

        return response.content.decode()

    def get_elements(self, element_class: MidpointObjectType) -> Element:
        url = urljoin(self.configuration.url, 'ws/rest/' + element_class.endpoint)
        response = self.requests_session.get(url=url, auth=(self.configuration.username, self.configuration.password))
        if response.status_code >= 300:
            raise MidpointServerError('Server responded with status code %d on %s' % (response.status_code, url))

        tree = ElementTree.fromstring(response.content)
        return tree

    def execute_action(self, element_class: MidpointObjectType, element_oid: str, action: str) -> bytes:
        response = self.requests_session.post(
            url=urljoin(self.configuration.url, 'ws/rest/' + element_class.endpoint + '/' + element_oid + '/' + action),
            auth=(self.configuration.username, self.configuration.password))

        return response.content

    def put_element(self, xml_filename: str, patch_file: str, patch_write: bool) -> Tuple[MidpointObjectType, str]:
        tree_root = self._load_xml(xml_filename)

        root_tag_name = tree_root.tag.split('}', 1)[1] if '}' in tree_root.tag else tree_root.tag  # strip namespace
        object_type = MidpointObjectTypes.find_by_tagname(root_tag_name)

        if not object_type.endpoint:
            raise MidpointUnsupportedOperation(f'Upload of {object_type} is not supported through REST API')

        object_oid = tree_root.attrib['oid']

        with open(xml_filename, 'r') as xml_file:
            xml_body = xml_file.read()

            if patch_file is not None:
                xml_body = patch_from_file(xml_body, patch_file, patch_write)

            target_url = urljoin(self.configuration.url, 'ws/rest/' + object_type.endpoint + '/' + object_oid)
            res = self.requests_session.put(
                url=target_url,
                data=xml_body,
                headers={'Content-Type': 'application/xml'},
                auth=(self.configuration.username, self.configuration.password))

            if res.status_code >= 300:
                raise MidpointServerError(f'Error {res.status_code} received from server at {target_url}')

            return object_type, object_oid

    @staticmethod
    def _load_xml(xml_file: str) -> (Element, dict):
        tree_root = ElementTree.parse(xml_file).getroot()
        return tree_root


class TaskExecutionFailure(Exception):
    def __init__(self, message: str):
        super(TaskExecutionFailure).__init__()
        self.message = message

    def __repr__(self):
        return self.message


class MidpointClient:

    def __init__(self, client_configuration: MidpointClientConfiguration = None, api_client: RestApiClient = None,
                 observer: MidpointCommunicationObserver = None):
        if client_configuration is not None:
            self.api_client = RestApiClient(client_configuration, observer=observer)

        if api_client is not None:
            self.api_client = api_client

    def __get_object(self, midpoint_type: MidpointObjectType, oid: str) -> Element:
        response = self.api_client.get_element(midpoint_type, oid)
        tree = ElementTree.fromstring(response)

        status = tree.find('c:status', namespaces)

        if status is not None and status.text == 'fatal_error':
            message = tree.find('c:partialResults/c:message', namespaces).text
            raise MidpointServerError(message)

        return tree

    def __get_collection(self, mp_class: MidpointObjectType, local_class: Type) -> MidpointObjectList:
        tree = self.api_client.get_elements(mp_class)
        return MidpointObjectList([local_class(entity) for entity in tree])

    def get_tasks(self) -> MidpointObjectList:
        return self.__get_collection(MidpointObjectTypes.TASK.value, MidpointTask)

    def get_resources(self) -> MidpointObjectList:
        return self.__get_collection(MidpointObjectTypes.RESOURCE.value, MidpointResource)

    def get_connectors(self) -> MidpointObjectList:
        return self.__get_collection(MidpointObjectTypes.CONNECTOR.value, MidpointConnector)

    def get_user(self, oid: str) -> Optional[MidpointUser]:
        user_root = self.__get_object(MidpointObjectTypes.USER.value, oid)
        user = MidpointUser(user_root)
        return user

    def get_users(self) -> MidpointObjectList:
        return self.__get_collection(MidpointObjectTypes.USER.value, MidpointUser)

    def get_orgs(self) -> MidpointObjectList:
        return self.__get_collection(MidpointObjectTypes.ORG.value, MidpointOrganization)

    def task_action(self, task_oid: str, task_action: str) -> Optional[MidpointTask]:
        self.api_client.execute_action(MidpointObjectTypes.TASK.value, task_oid, task_action)

        if task_action == 'run':
            return self.task_wait(task_oid)

    def task_wait(self, task_oid: str) -> MidpointTask:
        with AsciiProgressMonitor() as progress:
            while True:
                time.sleep(2)
                task_root = self.__get_object(MidpointObjectTypes.TASK.value, task_oid)
                task = MidpointTask(task_root)

                progress.update(int(task['Progress'] or '0'))

                rstatus = task['Result Status']

                if rstatus != 'in_progress':
                    print()
                    if rstatus != 'success':
                        raise TaskExecutionFailure('Failed execution of task ' + task_oid + ' with status ' + rstatus)

                    return task

    def test_resource(self, resource_oid: str) -> None:
        response = self.api_client.execute_action(MidpointObjectTypes.RESOURCE.value, resource_oid, 'test')
        tree = ElementTree.fromstring(response)
        status = tree.find('c:status', namespaces).text
        return status

    def get_xml(self, midpoint_type: MidpointObjectType, oid: str) -> Optional[str]:
        return self.api_client.get_element(midpoint_type, oid)

    def put_xml(self, xml_file: str, patch_file: str = None, patch_write: bool = False) -> Tuple[MidpointObjectType, str]:
        return self.api_client.put_element(xml_file, patch_file, patch_write)

    def delete(self, midpoint_type: MidpointObjectType, oid: str) -> str:
        return self.api_client.delete(midpoint_type, oid)
