#!/bin/bash

set -e -x

SANDBOX_BASE="$(dirname $(readlink -f ${0}))"
PROJECT_BASE="$(readlink -f ${SANDBOX_BASE}/../..)"

export MIDPOINT_PASSWORD=5ecR3t4ever
export MP_SET_midpoint_administrator_initialPassword=${MIDPOINT_PASSWORD}

MPCLI="poetry run midpoint-cli"

cd "${PROJECT_BASE}"

# Download distribution if missing
MP_VERSION=4.8.5
MP_DIST_FOLDER=midpoint-${MP_VERSION}
MP_DIST_ARCHIVE=midpoint-${MP_VERSION}-dist.tar.gz
test -f ${MP_DIST_ARCHIVE} || wget --progress=dot:giga https://evolveum.com/downloads/midpoint/${MP_VERSION}/${MP_DIST_ARCHIVE}

start_service() {
    rm -rf ${MP_DIST_FOLDER}
    tar xvf ${MP_DIST_ARCHIVE}
    ./${MP_DIST_FOLDER}/bin/midpoint.sh start
    ln -sf ${SANDBOX_BASE} /tmp

    ${MPCLI} put ${SANDBOX_BASE}/People-5k-Resource.xml
    ${MPCLI} put ${SANDBOX_BASE}/People-5k-ImportTask.xml
    ${MPCLI} task run Import5k
}

stop_service() {
    ./${MP_DIST_FOLDER}/bin/midpoint.sh stop || echo "Could not stop Midpoint service."
    rm -rf ${MP_DIST_FOLDER}
}

restart_service() {
    echo "Restarting service..."
    stop_service
    start_service
}

# Check the input argument
case "$1" in
    start)
        start_service
        ;;
    stop)
        stop_service
        ;;
    restart)
        restart_service
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
        ;;
esac
