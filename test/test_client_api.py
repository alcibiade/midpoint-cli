import unittest

from midpoint_cli.client import RestApiClient, MidpointClient, MidpointClientConfiguration, MidpointObjectTypes

test_configuration = MidpointClientConfiguration(
    url='cli-url',
    username='cli-usr',
    password='cli-pwd'
)


class ClientApiTest(unittest.TestCase):
    def test_url_sanitization(self):
        client = RestApiClient(test_configuration)
        self.assertEqual(client.configuration, test_configuration)

    def test_rest_types(self):
        self.assertEqual(MidpointObjectTypes.find_by_java_type(MidpointObjectTypes.USER.value.object_type),
                         MidpointObjectTypes.USER.value)
        self.assertEqual(MidpointObjectTypes.find_by_endpoint(MidpointObjectTypes.USER.value.endpoint),
                         MidpointObjectTypes.USER.value)
        self.assertEqual(MidpointObjectTypes.find_by_tagname(MidpointObjectTypes.USER.value.tagname),
                         MidpointObjectTypes.USER.value)

    def test_client_from_namespace(self):
        client = MidpointClient(test_configuration)
        self.assertEqual(test_configuration, client.api_client.configuration)

    def test_client_from_object(self):
        api_client = RestApiClient(test_configuration)
        client = MidpointClient(api_client=api_client)
        self.assertEqual(api_client, client.api_client)

    def test_client_from_priority(self):
        api_client = RestApiClient(test_configuration)
        client = MidpointClient(api_client=api_client, client_configuration=test_configuration)
        self.assertEqual(api_client, client.api_client)


if __name__ == '__main__':
    unittest.main()
