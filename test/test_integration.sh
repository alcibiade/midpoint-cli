#!/bin/bash

set -e -x

TEST_BASE="$(dirname $(readlink -f ${0}))"
PROJECT_BASE="$(readlink -f ${TEST_BASE}/..)"

cd "${PROJECT_BASE}"

MP_VERSION=$1

MP_DIST_FOLDER=midpoint-${MP_VERSION}
MP_DIST_ARCHIVE=midpoint-${MP_VERSION}-dist.tar.gz

# Delete potentially already existing distribution folder
rm -rf ${MP_DIST_FOLDER}

# Show current folder content before starting the actual work
ls -l

# Download distribution if missing
test -f ${MP_DIST_ARCHIVE} || wget --progress=dot:giga https://evolveum.com/downloads/midpoint/${MP_VERSION}/${MP_DIST_ARCHIVE}

# Extract distribution
tar xf ${MP_DIST_ARCHIVE}

if expr "$MP_VERSION" : "^4.[89]" > /dev/null; then
    export MP_PASS=5ecR3t4ever
    export SANDBOX=test/sandbox-environment-4.8
else
    export MP_PASS=5ecr3t
    export SANDBOX=test/sandbox-environment
fi

export MP_SET_midpoint_administrator_initialPassword=${MP_PASS}

export PATH=$PATH:$PWD/${MP_DIST_FOLDER}/bin

# For version 4.9 we force a PostgreSQL configuration and Ninja initialization
if expr "$MP_VERSION" : "^4.9" > /dev/null; then
    cp test/config-4.9.xml ${MP_DIST_FOLDER}/var/config.xml
    pushd ${MP_DIST_FOLDER}
    ninja.sh run-sql --create --mode REPOSITORY
    ninja.sh run-sql --create --mode AUDIT
    popd
fi

midpoint.sh start

cp ${SANDBOX}/original-repository1.csv /tmp/repository1.csv
cp ${SANDBOX}/original-repository2.csv /tmp/repository2.csv
echo 'assert len(client.get_users()) == 1' | midpoint-cli -p ${MP_PASS} script run -
midpoint-cli -p ${MP_PASS} user ls
midpoint-cli -p ${MP_PASS} put ${SANDBOX}/object-template-user.xml
midpoint-cli -p ${MP_PASS} put ${SANDBOX}/resource-repository1.xml
midpoint-cli -p ${MP_PASS} resource ls
midpoint-cli -p ${MP_PASS} resource test \"Repository 1\"
midpoint-cli -p ${MP_PASS} put ${SANDBOX}/resource-repository1-Import.xml
midpoint-cli -p ${MP_PASS} task ls
midpoint-cli -p ${MP_PASS} task run \"R1 Import\"
midpoint-cli -p ${MP_PASS} user ls
midpoint-cli -p ${MP_PASS} user get 00000000-0000-0000-0000-000000000002

echo 'assert len(client.get_users()) == 3' | midpoint-cli -p ${MP_PASS} script run -
echo "assert 'Hannibal Smith' in [u['FullName'] for u in client.get_users()]" | midpoint-cli -p ${MP_PASS} script run -
echo "assert 'hannibal.smith@mercernary4hire.com' in [u['Email'] for u in client.get_users()]" | midpoint-cli -p ${MP_PASS} script run -
midpoint.sh stop
